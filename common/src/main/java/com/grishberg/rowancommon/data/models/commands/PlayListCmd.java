package com.grishberg.rowancommon.data.models.commands;

import android.os.Parcel;
import android.os.Parcelable;

import com.grishberg.rowancommon.mediaPlayer.TrackItem;
import com.grishberg.rowancommon.mediaPlayer.TrackList;
import com.grishberg.rowancommon.mediaPlayer.TrackRelation;

import java.util.List;

/**
 * Created by grishberg on 20.06.16.
 * Команда для отправки плейлиста
 */
public class PlayListCmd implements Parcelable {
    private static final String TAG = PlayListCmd.class.getSimpleName();
    private int id;
    private String name;
    private List<TrackRelation> trackList;

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public List<TrackRelation> getTrackList() {
        return trackList;
    }

    public String getName() {
        return name;
    }

    public void setTrackList(List<TrackRelation> trackList) {
        this.trackList = trackList;
    }

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeInt(this.id);
        dest.writeString(this.name);
        dest.writeTypedList(this.trackList);
    }

    public PlayListCmd() {
    }

    public PlayListCmd(int id, String name, List<TrackRelation> trackList) {
        this.id = id;
        this.name = name;
        this.trackList = trackList;
    }

    protected PlayListCmd(Parcel in) {
        this.id = in.readInt();
        this.name = in.readString();
        this.trackList = in.createTypedArrayList(TrackRelation.CREATOR);
    }

    public static final Creator<PlayListCmd> CREATOR = new Creator<PlayListCmd>() {
        @Override
        public PlayListCmd createFromParcel(Parcel source) {
            return new PlayListCmd(source);
        }

        @Override
        public PlayListCmd[] newArray(int size) {
            return new PlayListCmd[size];
        }
    };
}
