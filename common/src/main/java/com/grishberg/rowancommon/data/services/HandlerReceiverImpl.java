package com.grishberg.rowancommon.data.services;

import android.app.Service;
import android.content.Intent;
import android.os.Binder;
import android.os.Handler;
import android.os.IBinder;
import android.os.Message;

import com.grishberg.rowancommon.data.models.MessagesContainer;
import com.grishberg.rowancommon.interfaces.HandlerReceiver;

import java.util.Iterator;
import java.util.Queue;
import java.util.concurrent.ConcurrentLinkedQueue;

/**
 * Created by grishberg on 20.05.16.
 */
public abstract class HandlerReceiverImpl extends Service implements HandlerReceiver {
    private static final String TAG = HandlerReceiverImpl.class.getSimpleName();
    protected Handler activityHandler;
    protected final Queue<MessagesContainer> messagesQueue = new ConcurrentLinkedQueue<>();
    private LocalBinder binder = new LocalBinder();


    /**
     * Регистрация хэндлера для взаимодействия с активностью
     *
     * @param handler
     */
    public void registerHandler(Handler handler) {
        this.activityHandler = handler;
        Iterator<MessagesContainer> iterator = messagesQueue.iterator();
        while (iterator.hasNext()) {
            sendMessageToActivity(iterator.next());
            iterator.remove();
        }
    }

    /**
     * снятие регистрации хэндлера
     *
     * @param handler
     */
    public void unregisterHandler(Handler handler) {
        this.activityHandler = null;
    }

    /**
     * Отправить сообщение активности, либо добавить в пул сообщений
     *
     * @param msg
     */
    protected void sendMessageToActivity(MessagesContainer msg) {
        if (activityHandler != null) {
            Message message = activityHandler.obtainMessage(msg.getAction(), msg.getData());
            activityHandler.sendMessage(message);
        } else {
            messagesQueue.add(msg);
        }
    }


    @Override
    public IBinder onBind(Intent intent) {
        // TODO: Return the communication channel to the service.
        return binder;
    }

    /**
     * Binder для взаимодействия с активностью
     */
    public class LocalBinder extends Binder {
        public HandlerReceiverImpl getService() {
            // Return this instance of LocalService so clients can call public methods
            return HandlerReceiverImpl.this;
        }
    }
}
