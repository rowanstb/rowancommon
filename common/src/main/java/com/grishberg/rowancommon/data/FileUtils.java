package com.grishberg.rowancommon.data;

import android.content.Context;
import android.database.Cursor;
import android.graphics.Path;
import android.net.Uri;
import android.provider.MediaStore;
import android.util.Log;

import java.io.ByteArrayOutputStream;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;

/**
 * Created by grishberg on 20.05.16.
 */
public class FileUtils {
    private static final String TAG = FileUtils.class.getSimpleName();

    public static File getRealPathFromUri(Context context, Uri uri) {
        Cursor cursor = null;
        try {
            cursor = context.getContentResolver().query(uri, null, null, null, null);
            if (cursor != null && !cursor.isClosed()) {
                cursor.moveToFirst();
                int idx = cursor.getColumnIndex(MediaStore.Images.ImageColumns.DATA);
                return new File(cursor.getString(idx));
            }
            return new File(uri.getPath());
        } finally {
            if (cursor != null && !cursor.isClosed()) {
                cursor.close();
            }
        }
    }

    public static byte[] readFile(File file) {
        ByteArrayOutputStream ous = new ByteArrayOutputStream();
        InputStream ios = null;
        try {
            byte[] buffer = new byte[4096];
            ios = new FileInputStream(file);
            int read = 0;
            while ((read = ios.read(buffer)) != -1) {
                ous.write(buffer, 0, read);
            }
        } catch (IOException e) {
            Log.e(TAG, "read: ", e);
        } finally {
            try {
                if (ous != null)
                    ous.close();
            } catch (IOException e) {
            }

            try {
                if (ios != null)
                    ios.close();
            } catch (IOException e) {
            }
        }
        return ous.toByteArray();
    }

    /**
     * Сохранить файл
     *
     * @param file
     * @param data буффер для сохранения в файл
     * @return
     */
    public static boolean saveToFile(File file, byte[] data) {
        FileOutputStream fos = null;
        try {
            if (file.exists()) {
                file.delete();
            }
            fos = new FileOutputStream(file);
            fos.write(data, 0, data.length);
            fos.close();
            return true;
        } catch (IOException e) {
            Log.e(TAG, "saveFile: ", e);
        } finally {
            if (fos != null) {
                try {
                    fos.close();
                } catch (IOException e) {
                    e.printStackTrace();
                }
            }
        }
        return false;
    }
}
