package com.grishberg.rowancommon.data.db;

/**
 * Created by grishberg on 19.06.16.
 */

import android.support.annotation.NonNull;
import android.util.Log;

import com.grishberg.datafacade.data.BaseResult;
import com.grishberg.datafacade.data.DataReceiveObserver;

import java.util.ArrayList;
import java.util.List;

import io.realm.Realm;
import io.realm.RealmChangeListener;
import io.realm.RealmModel;
import io.realm.RealmObject;
import io.realm.RealmResults;

/**
 * Created by g on 20.04.16.
 */
public abstract class BaseResultImpl<T extends RealmModel> implements BaseResult, RealmChangeListener<RealmResults<T>> {
    private static final String TAG = BaseResultImpl.class.getSimpleName();
    private final List<DataReceiveObserver> observers;
    protected RealmResults<T> realmResults;
    // через данный листенер будет возвращаться небоходимый запрос
    protected final GetRequestListener<T> getRequestListener;
    protected final Realm realm;

    public BaseResultImpl(Realm realm, @NonNull GetRequestListener<T> listener) {
        getRequestListener = listener;
        this.realmResults = getRequestListener.getRequest();
        observers = new ArrayList<>();
        this.realm = realm;
        realmResults.addChangeListener(this);
        Log.d(TAG, "BaseResultImpl: isLoaded = " + realmResults.isLoaded());
        if (realmResults.isLoaded()) {
            onChange(realmResults);
        }
    }

    /**
     * Добавить слушателя
     *
     * @param observer
     */
    @Override
    public void addDataReceiveObserver(DataReceiveObserver observer) {
        observers.add(observer);
    }

    /**
     * удалить слушателя
     *
     * @param observer
     */
    @Override
    public void removeDataReceiveObserver(DataReceiveObserver observer) {
        observers.remove(observer);
    }

    /**
     * Сообщить подписчикам о том что данные обновлены
     */
    @Override
    public void onChange(RealmResults<T> element) {
        Log.d(TAG, "onChange: ");
        if (element == null) {
            Log.e(TAG, "onChange: elements is null");
            return;
        }
        onDataChanged(element);
        for (DataReceiveObserver observer : observers) {
            observer.onDataReceived();
        }
    }

    /**
     * Вызывается когда приходят новые данные из RealmResult
     *
     * @param elements
     */
    protected abstract void onDataChanged(RealmResults<T> elements);

    @Override
    public boolean isLoaded() {
        return false;
    }

    @Override
    public void release() {
        if (realmResults != null) {
            realmResults.removeChangeListener(this);
        }
    }
}
