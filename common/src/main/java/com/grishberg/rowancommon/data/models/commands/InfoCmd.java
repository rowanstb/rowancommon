package com.grishberg.rowancommon.data.models.commands;

import android.os.Parcel;
import android.os.Parcelable;
import android.support.annotation.IntDef;

/**
 * Created by grishberg on 18.05.16.
 */
public class InfoCmd implements Parcelable {
    private static final String TAG = InfoCmd.class.getSimpleName();
    public static final int CHECK_FILE_EXISTS = 1;
    public static final int GET_CURRENT_PLAYLIST = 2;
    public static final int GET_CURRENT_TRACK = 3;
    public static final int CURRENT_TRACK = 4;
    public static final int SWAP_TRACK = 5;

    @IntDef({CHECK_FILE_EXISTS, GET_CURRENT_PLAYLIST,
            GET_CURRENT_TRACK,
            CURRENT_TRACK,
            SWAP_TRACK})
    public @interface InfoCmdType {
    }

    private int id;
    /**
     * Тип информационной команды
     */
    @InfoCmdType
    private int type;
    private String dataStr;
    private int dataInt;
    private int dataInt2;

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeInt(this.id);
        dest.writeInt(this.type);
        dest.writeString(this.dataStr);
        dest.writeInt(this.dataInt);
        dest.writeInt(this.dataInt2);
    }

    public InfoCmd() {
    }

    public InfoCmd(int id, @InfoCmdType int type) {
        this.id = id;
        this.type = type;
    }

    public InfoCmd(int id, @InfoCmdType int type, String dataStr) {
        this.id = id;
        this.type = type;
        this.dataStr = dataStr;
    }

    public InfoCmd(int id, @InfoCmdType int type, int dataInt) {
        this.id = id;
        this.type = type;
        this.dataInt = dataInt;
        this.dataStr = null;
    }

    /**
     * Для отправки команды перемещения трэка
     * @param id
     * @param type
     * @param dataInt идентификатор перемещаемого элемента
     * @param dataInt2 новая позиция элемента
     */
    public InfoCmd(int id, @InfoCmdType int type, int dataInt, int dataInt2) {
        this.id = id;
        this.type = type;
        this.dataInt = dataInt;
        this.dataInt2 = dataInt2;
        this.dataStr = null;
    }
    protected InfoCmd(Parcel in) {
        this.id = in.readInt();
        switch (in.readInt()) {
            case CHECK_FILE_EXISTS:
                this.type = CHECK_FILE_EXISTS;
                break;
            case GET_CURRENT_PLAYLIST:
                this.type = GET_CURRENT_PLAYLIST;
                break;
            case GET_CURRENT_TRACK:
                this.type = GET_CURRENT_TRACK;
                break;
            case CURRENT_TRACK:
                this.type = CURRENT_TRACK;
                break;
            case SWAP_TRACK:
                this.type = SWAP_TRACK;
                break;
        }
        this.dataStr = in.readString();
        this.dataInt = in.readInt();
        this.dataInt2 = in.readInt();
    }

    public static final Creator<InfoCmd> CREATOR = new Creator<InfoCmd>() {
        @Override
        public InfoCmd createFromParcel(Parcel source) {
            return new InfoCmd(source);
        }

        @Override
        public InfoCmd[] newArray(int size) {
            return new InfoCmd[size];
        }
    };

    public int getId() {
        return id;
    }

    public
    @InfoCmdType
    int getType() {
        return type;
    }

    public String getDataStr() {
        return dataStr;
    }

    public int getDataInt() {
        return dataInt;
    }

    public int getDataInt2() {
        return dataInt2;
    }
}
