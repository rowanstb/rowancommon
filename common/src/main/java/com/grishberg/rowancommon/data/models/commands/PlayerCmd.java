package com.grishberg.rowancommon.data.models.commands;

import android.os.Parcel;
import android.os.Parcelable;
import android.support.annotation.IntDef;

import java.lang.annotation.Retention;
import java.lang.annotation.RetentionPolicy;

/**
 * Created by grishberg on 15.05.16.
 * Команда для плеера
 */
public class PlayerCmd implements Parcelable {
    private static final String TAG = PlayerCmd.class.getSimpleName();
    public static final int CMD_PLAY = 1;
    public static final int CMD_PAUSE = 2;
    public static final int CMD_NEXT = 3;
    public static final int CMD_PREV = 4;
    public static final int CMD_SEEK = 5;
    public static final int CMD_VOLUME = 6;
    public static final int CMD_PLAY_URL = 7;
    public static final int CMD_STOP = 8;

    // play, pause, next, prev, seek, volume
    @Retention(RetentionPolicy.SOURCE)
    @IntDef({CMD_PLAY,
            CMD_PAUSE,
            CMD_NEXT,
            CMD_PREV,
            CMD_SEEK,
            CMD_VOLUME,
            CMD_PLAY_URL,
            CMD_STOP})
    public @interface PlayerCmdType {
    }

    @PlayerCmdType
    private int cmd;
    private int parameter1;
    private String parameter2;

    public PlayerCmd() {
    }

    public PlayerCmd(@PlayerCmdType int cmd) {
        this.cmd = cmd;
    }

    public PlayerCmd(@PlayerCmdType int cmd, int parameter1, String parameter2) {
        this.cmd = cmd;
        this.parameter1 = parameter1;
        this.parameter2 = parameter2;
    }

    public PlayerCmd(@PlayerCmdType int cmd, int parameter1) {
        this.cmd = cmd;
        this.parameter1 = parameter1;
    }

    public PlayerCmd(@PlayerCmdType int cmd, String parameter2) {
        this.cmd = cmd;
        this.parameter2 = parameter2;
    }

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeInt(this.cmd);
        dest.writeInt(this.parameter1);
        dest.writeString(this.parameter2);
    }

    protected PlayerCmd(Parcel in) {
        switch (in.readInt()) {
            case CMD_PLAY:
                this.cmd = CMD_PLAY;
                break;
            case CMD_PAUSE:
                this.cmd = CMD_PAUSE;
                break;
            case CMD_NEXT:
                this.cmd = CMD_NEXT;
                break;
            case CMD_PREV:
                this.cmd = CMD_PREV;
                break;
            case CMD_SEEK:
                this.cmd = CMD_SEEK;
                break;
            case CMD_STOP:
                this.cmd = CMD_STOP;
                break;
            case CMD_VOLUME:
                this.cmd = CMD_VOLUME;
                break;
            case CMD_PLAY_URL:
                this.cmd = CMD_PLAY_URL;
                break;
        }
        this.parameter1 = in.readInt();
        this.parameter2 = in.readString();
    }

    public static final Creator<PlayerCmd> CREATOR = new Creator<PlayerCmd>() {
        @Override
        public PlayerCmd createFromParcel(Parcel source) {
            return new PlayerCmd(source);
        }

        @Override
        public PlayerCmd[] newArray(int size) {
            return new PlayerCmd[size];
        }
    };

    public int getCmd() {
        return cmd;
    }

    public int getParameter1() {
        return parameter1;
    }

    public String getParameter2() {
        return parameter2;
    }
}
