package com.grishberg.rowancommon.data.db;

import android.util.Log;

import com.grishberg.datafacade.data.ListResult;
import com.grishberg.datafacade.data.SingleResult;
import com.grishberg.rowancommon.data.models.commands.SendFileCmd;
import com.grishberg.rowancommon.mediaPlayer.TrackItem;
import com.grishberg.rowancommon.mediaPlayer.TrackList;
import com.grishberg.rowancommon.mediaPlayer.TrackRelation;

import java.io.File;
import java.util.List;

import io.realm.Realm;
import io.realm.RealmResults;
import io.realm.Sort;

/**
 * Created by grishberg on 19.06.16.
 */
public class DbServiceRealmImpl implements DbService {
    private static final String TAG = DbServiceRealmImpl.class.getSimpleName();
    public static final String DEFAULT_PLAYLIST_NAME = "";
    private Realm realm;

    public DbServiceRealmImpl() {
        this.realm = Realm.getDefaultInstance();
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public ListResult<TrackList> getTrackLists() {

        ListResultRealm<TrackList> result = new ListResultRealm<>(realm, new GetRequestListener<TrackList>() {
            @Override
            public RealmResults<TrackList> getRequest() {
                return realm.where(TrackList.class).findAllSortedAsync("name", Sort.ASCENDING);
            }
        });
        return result;
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public int addTrackItem(final File filePath, final SendFileCmd fileCmd, final boolean isEnabled) {
        Log.d(TAG, "addTrackItem: " + fileCmd.getFileName());
        final int[] result = new int[1];
        Realm realm = Realm.getDefaultInstance();
        realm.executeTransaction(new Realm.Transaction() {
            @Override
            public void execute(Realm realm) {
                //Текущий трэклист
                TrackList trackList = getCurrentPlayList();

                if (trackList == null) {
                    trackList = new TrackList();
                    trackList.setName("");
                    trackList = realm.copyToRealmOrUpdate(trackList);
                }
                // файл не найден, можно добавить
                TrackItem trackItem = realm.where(TrackItem.class)
                        .equalTo("fileName", fileCmd.getFileName()).findFirst();
                // поиск существующего файла в базе
                if (trackItem == null) {
                    // создать новый трэк
                    trackItem = addTrackItem(realm, fileCmd, filePath.getAbsolutePath(), isEnabled);
                }
                // поиск существующей привязки к плейлисту
                TrackRelation trackRelation = realm.where(TrackRelation.class)
                        .equalTo("playListName", trackList.getName())
                        .equalTo("trackItem.id", trackItem.getId())
                        .findFirst();
                if (trackRelation == null) {
                    // добавление привязки к плейлисту
                    trackList.getTracks().add(addTrackListRelation(realm, trackList, trackItem));
                    result[0] = trackItem.getId();
                }
            }
        });

        realm.close();
        return result[0];
    }

    /**
     * Создать привязку трэка к плейлисту
     *
     * @param realm
     * @param trackList
     * @param trackItem
     */
    private TrackRelation addTrackListRelation(Realm realm, TrackList trackList, TrackItem trackItem) {
        TrackRelation trackRelation;
        trackRelation = new TrackRelation();
        int position = 0;
        Number numberPosition = realm.where(TrackRelation.class)
                .equalTo("playListName", trackList.getName())
                .max("position");
        if (numberPosition != null) {
            position = numberPosition.intValue() + 1;
        }
        trackRelation.setPosition(position);
        trackRelation.setPlayListName(trackList.getName());
        trackRelation.setTrackItem(trackItem);
        return realm.copyToRealm(trackRelation);
    }

    /**
     * Добавить новый трэк в бд
     *
     * @param realm
     * @param fileCmd
     * @param isEnabled
     * @return
     */
    private TrackItem addTrackItem(Realm realm, SendFileCmd fileCmd, String path, boolean isEnabled) {
        int id = 1;
        Number numberId = realm.where(TrackItem.class).max("id");
        if (numberId != null) {
            id = numberId.intValue() + 1;
        }
        TrackItem trackItem = new TrackItem();
        trackItem.setId(id);
        trackItem.setFileName(path);
        trackItem.setArtist(fileCmd.getArtist());
        trackItem.setName(fileCmd.getTrackName());
        trackItem.setGenre(fileCmd.getGenre());
        trackItem.setEnabled(isEnabled);
        trackItem = realm.copyToRealmOrUpdate(trackItem);
        return trackItem;
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public void acceptTrackItem(final String trackName) {
        Realm realm = Realm.getDefaultInstance();
        realm.executeTransaction(new Realm.Transaction() {
            @Override
            public void execute(Realm realm) {
                TrackItem trackItem = realm.where(TrackItem.class)
                        .equalTo("fileName", trackName).findFirst();

                if (trackItem != null) {
                    trackItem.setEnabled(true);
                }
            }
        });
        realm.close();
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public void removeTrackItem(final String trackListName, final String trackName) {
        Realm realm = Realm.getDefaultInstance();
        realm.executeTransaction(new Realm.Transaction() {
            @Override
            public void execute(Realm realm) {
                TrackList trackList = realm.where(TrackList.class)
                        .equalTo("name", trackListName).findFirst();

                if (trackList == null) {
                    return;
                }
                trackList.getTracks().where().equalTo("trackName", trackName)
                        .findAll().deleteAllFromRealm();
            }
        });
        realm.close();
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public List<TrackRelation> getTrackItems(final String trackListName) {
        final Realm realm = Realm.getDefaultInstance();
        try {

            return realm.copyFromRealm(
                    realm.where(TrackRelation.class)
                            .equalTo("playListName", trackListName)
                            .findAllSorted("position", Sort.ASCENDING));
        } finally {
            realm.close();
        }
    }

    @Override
    public TrackList getCurrentPlayList() {
        final Realm realm = Realm.getDefaultInstance();
        try {
            TrackList res = realm.where(TrackList.class).findFirst();
            if (res == null) {
                addNewTrackList(DEFAULT_PLAYLIST_NAME);
                res = realm.where(TrackList.class).findFirst();
            }
            return realm.copyFromRealm(res);
        } finally {
            if (!realm.isClosed()) {
                realm.close();
            }
        }
    }

    @Override
    public void addNewTrackList(final String name) {
        Realm realm = Realm.getDefaultInstance();
        realm.executeTransaction(new Realm.Transaction() {
            @Override
            public void execute(Realm realm) {
                TrackList trackList = new TrackList();
                trackList.setName(name);
                realm.copyToRealmOrUpdate(trackList);
            }
        });
        realm.close();
    }

    @Override
    public void swapPosition(final int fromId, final int toPos) {
        Realm realm = Realm.getDefaultInstance();
        realm.executeTransaction(new Realm.Transaction() {
            @Override
            public void execute(Realm realm) {
                TrackList trackList = getCurrentPlayList();
                if (trackList == null) {
                    Log.e(TAG, "swapPosition: default playlist not found");
                    return;
                }
                // поменять позицию у целевого трэка
                TrackRelation targetTrack = realm.where(TrackRelation.class)
                        .equalTo("trackItem.id", fromId).findFirst();
                if (targetTrack == null) {
                    return;
                }
                boolean movingDown = targetTrack.getPosition() < toPos;
                int upBound = movingDown ? targetTrack.getPosition() : toPos;
                int downBound = !movingDown ? targetTrack.getPosition() : toPos;
                targetTrack.setPosition(toPos);
                // упорядоченный список элементов, у которых небходимо поменять позицию
                RealmResults<TrackRelation> tracks = realm.where(TrackRelation.class)
                        .equalTo("playListName", trackList.getName())
                        .greaterThanOrEqualTo("position", upBound)
                        .lessThanOrEqualTo("position", downBound)
                        .notEqualTo("trackItem.id", fromId)
                        .findAllSorted("position");
                for (int i = 0; i < tracks.size(); i++) {
                    TrackRelation currentTrack = tracks.get(i);
                    if (movingDown) {
                        // при перемещении вниз,
                        currentTrack.setPosition(currentTrack.getPosition() - 1);
                        continue;
                    }
                    currentTrack.setPosition(currentTrack.getPosition() + 1);
                }
            }
        });
        realm.close();
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public void release() {
        Log.d(TAG, "release: ");
        if (realm != null && !realm.isClosed()) {
            realm.close();
        }
    }
}
