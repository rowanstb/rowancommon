package com.grishberg.rowancommon.data.db;

import io.realm.RealmModel;
import io.realm.RealmObject;
import io.realm.RealmResults;

/**
 * Created by g on 17.06.16.
 * Интерфейс возвращает RealmResult нужной модели
 */
public interface GetRequestListener<T extends RealmModel> {
    RealmResults<T> getRequest();
}
