package com.grishberg.rowancommon.data.models.commands;

import android.os.Parcel;
import android.os.Parcelable;

/**
 * Created by grishberg on 15.05.16.
 */
public class KeyPressCmd implements Parcelable {
    private static final String TAG = KeyPressCmd.class.getSimpleName();
    private int key;
    private boolean isDown;

    public KeyPressCmd(int key, boolean isDown) {
        this.key = key;
        this.isDown = isDown;
    }

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeInt(this.key);
        dest.writeByte(isDown ? (byte) 1 : (byte) 0);
    }

    public KeyPressCmd() {
    }

    protected KeyPressCmd(Parcel in) {
        this.key = in.readInt();
        this.isDown = in.readByte() != 0;
    }

    public static final Creator<KeyPressCmd> CREATOR = new Creator<KeyPressCmd>() {
        @Override
        public KeyPressCmd createFromParcel(Parcel source) {
            return new KeyPressCmd(source);
        }

        @Override
        public KeyPressCmd[] newArray(int size) {
            return new KeyPressCmd[size];
        }
    };

    public int getKey() {
        return key;
    }

    public boolean isDown() {
        return isDown;
    }
}
