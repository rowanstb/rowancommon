package com.grishberg.rowancommon.data.db;

import com.grishberg.datafacade.data.ListResult;
import com.grishberg.datafacade.data.SingleResult;
import com.grishberg.rowancommon.data.models.commands.SendFileCmd;
import com.grishberg.rowancommon.mediaPlayer.TrackItem;
import com.grishberg.rowancommon.mediaPlayer.TrackList;
import com.grishberg.rowancommon.mediaPlayer.TrackRelation;

import java.io.File;
import java.util.List;

/**
 * Created by grishberg on 19.06.16.
 */
public interface DbService {
    /**
     * вернуть список плейлистов
     * @return
     */
    ListResult<TrackList> getTrackLists();

    /**
     * Добавить трэк в бд
     * @param fileCmd
     * @param isEnabled признак того, что файл можно воспроизводить
     */
    int addTrackItem(File filePath, SendFileCmd fileCmd, boolean isEnabled);

    /**
     * разрешить воспроизводить трэк
     * @param trackName
     */
    void acceptTrackItem(String trackName);

    /**
     * удалить трэк из плейлиста
     * @param trackName
     */
    void removeTrackItem(String trackListName, String trackName);

    /**
     * вернуть трэки для плейлиста
     * @param trackListName
     * @return
     */
    List<TrackRelation> getTrackItems(String trackListName);

    /**
     * Вернуть последний плейлист
     * @return
     */
    TrackList getCurrentPlayList();

    /**
     * Создать новый трэклист
     * @param name
     */
    void addNewTrackList(String name);

    /**
     * Меняет позиции для текущего плейлиста
     * @param fromId идентификтор элемента, который будет переноситься
     * @param toPost новая позиция элемента
     */
    void swapPosition(int fromId, int toPost);
    /**
     * Очистка ресурсов, вызвать перед выходом из приложения
     */
    void release();


}
