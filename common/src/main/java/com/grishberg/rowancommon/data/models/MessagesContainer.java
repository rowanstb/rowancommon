package com.grishberg.rowancommon.data.models;

/**
 * Created by grishberg on 14.05.16.
 */
public class MessagesContainer {
    private static final String TAG = MessagesContainer.class.getSimpleName();
    private int action;
    private Object data;

    public MessagesContainer(int action, Object data) {
        this.action = action;
        this.data = data;
    }

    public int getAction() {
        return action;
    }

    public Object getData() {
        return data;
    }
}
