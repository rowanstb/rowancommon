package com.grishberg.rowancommon.data.db;

/**
 * Created by grishberg on 19.06.16.
 */

import com.grishberg.datafacade.data.SingleResult;

import java.util.concurrent.locks.Lock;
import java.util.concurrent.locks.ReentrantLock;

import io.realm.Realm;
import io.realm.RealmModel;
import io.realm.RealmObject;
import io.realm.RealmResults;

/**
 * Created by g on 20.04.16.
 */
public class SingleResultRealm<T extends RealmModel> extends BaseResultImpl<T>
        implements SingleResult<T> {
    private boolean isWaiting;
    private final Object monitor = new Object();

    public SingleResultRealm(Realm realm, GetRequestListener<T> requestListener) {
        super(realm, requestListener);
    }

    /**
     * Вернуть элемент
     *
     * @return
     */
    @Override
    public T getItem() {
        return realmResults.size() > 0 ? realmResults.first() : null;
    }

    @Override
    public T getItemSync() {
        if (isLoaded()) {
            return getItem();
        }
        synchronized (monitor) {
            isWaiting = true;
            try {
                while (isWaiting) {
                    monitor.wait();
                }
            } catch (InterruptedException e) {
                e.printStackTrace();
            }
        }
        return null;
    }

    /**
     * Вернуть состояние - получены ли данные из бд
     *
     * @return
     */
    @Override
    public boolean isLoaded() {
        return realmResults.isLoaded();
    }

    @Override
    protected void onDataChanged(RealmResults<T> elements) {
        synchronized (monitor) {
            if (isWaiting) {
                monitor.notifyAll();
            }
        }
    }
}