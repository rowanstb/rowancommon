package com.grishberg.rowancommon.data.models.commands;

import android.os.Parcel;
import android.os.Parcelable;
import android.support.annotation.IntDef;

import java.lang.annotation.Retention;
import java.lang.annotation.RetentionPolicy;

/**
 * Created by grishberg on 15.05.16.
 */
public class SendFileCmd implements Parcelable {
    private static final String TAG = SendFileCmd.class.getSimpleName();
    public static final int CURRENT_IMAGE = 1;
    public static final int BUFFERED_IMAGE = 2;
    public static final int AUDIO = 3;

    @Retention(RetentionPolicy.SOURCE)
    @IntDef({CURRENT_IMAGE,
            BUFFERED_IMAGE,
            AUDIO})
    public @interface FileType {
    }

    @FileType
    private int fileType;
    private String fileName;
    private String artist;
    private String trackName;
    private String genre;
    private byte[] data;

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeInt(this.fileType);
        dest.writeString(this.fileName);
        dest.writeString(this.artist);
        dest.writeString(this.trackName);
        dest.writeString(this.genre);
        dest.writeByteArray(this.data);
    }

    public SendFileCmd() {
    }

    /**
     * Конструктор для фото
     * @param fileType
     * @param fileName
     * @param data
     */
    public SendFileCmd(@FileType int fileType, String fileName, byte[] data) {
        this.fileType = fileType;
        this.fileName = fileName;
        this.data = data;
    }

    /**
     * Конструктор для аудиофайлов
     * @param fileType
     * @param fileName
     * @param artist
     * @param trackName
     * @param genre
     * @param data
     */
    public SendFileCmd(@FileType int fileType,
                       String fileName,
                       String artist,
                       String trackName,
                       String genre, byte[] data) {
        this.fileType = fileType;
        this.fileName = fileName;
        this.artist = artist;
        this.trackName = trackName;
        this.genre = genre;
        this.data = data;
    }

    protected SendFileCmd(Parcel in) {
        switch (in.readInt()){
            case CURRENT_IMAGE:
                this.fileType = CURRENT_IMAGE;
                break;
            case BUFFERED_IMAGE:
                this.fileType = BUFFERED_IMAGE;
                break;
            case AUDIO:
                this.fileType = AUDIO;
                break;

        }
        this.fileName = in.readString();
        this.artist = in.readString();
        this.trackName = in.readString();
        this.genre = in.readString();
        this.data = in.createByteArray();
    }

    public static final Creator<SendFileCmd> CREATOR = new Creator<SendFileCmd>() {
        @Override
        public SendFileCmd createFromParcel(Parcel source) {
            return new SendFileCmd(source);
        }

        @Override
        public SendFileCmd[] newArray(int size) {
            return new SendFileCmd[size];
        }
    };

    public @FileType int getFileType() {
        return fileType;
    }

    public String getFileName() {
        return fileName;
    }

    public String getArtist() {
        return artist;
    }

    public String getTrackName() {
        return trackName;
    }

    public String getGenre() {
        return genre;
    }

    public byte[] getData() {
        return data;
    }
}
