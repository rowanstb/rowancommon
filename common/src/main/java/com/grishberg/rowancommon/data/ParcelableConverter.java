package com.grishberg.rowancommon.data;

import android.os.Parcelable;
import android.util.Log;

import com.grishberg.rowancommon.data.models.commands.InfoCmd;
import com.grishberg.rowancommon.data.models.commands.KeyPressCmd;
import com.grishberg.rowancommon.data.models.commands.PlayListCmd;
import com.grishberg.rowancommon.data.models.commands.PlayerCmd;
import com.grishberg.rowancommon.data.models.commands.SendFileCmd;

import java.nio.ByteBuffer;

/**
 * Created by grishberg on 15.05.16.
 */
public class ParcelableConverter {
    private static final String TAG = ParcelableConverter.class.getSimpleName();
    private static final int TYPE_KEY_PRESS_CMD = 1;
    private static final int TYPE_SEND_FILE_CMD = 2;
    private static final int TYPE_PLAYER_CMD = 3;
    private static final int TYPE_PLAYLIST_CMD = 4;
    private static final int TYPE_INFO_CMD = 5;

    /**
     * Преобразование из объекта в массив байт
     *
     * @param data
     * @return
     */
    public static byte[] convertToArray(Parcelable data) {
        byte[] obj = ParcelableUtil.marshall(data);
        ByteBuffer buffer = ByteBuffer.allocate(obj.length + 8);
        if (data instanceof KeyPressCmd) {
            buffer.putInt(TYPE_KEY_PRESS_CMD);
        } else if (data instanceof SendFileCmd) {
            buffer.putInt(TYPE_SEND_FILE_CMD);
        } else if (data instanceof PlayerCmd) {
            buffer.putInt(TYPE_PLAYER_CMD);
        } else if (data instanceof PlayListCmd) {
            buffer.putInt(TYPE_PLAYLIST_CMD);
        } else if (data instanceof InfoCmd) {
            buffer.putInt(TYPE_INFO_CMD);
        }
        buffer.put(obj);
        return buffer.array();
    }

    /**
     * Преобразование из массива байт в объетк
     *
     * @param data
     * @return
     */
    public static Parcelable convertFromArray(byte[] data) {
        ByteBuffer buffer = ByteBuffer.wrap(data);
        byte[] bytes = new byte[data.length - 4];
        int type = buffer.getInt();
        buffer.get(bytes);
        switch (type) {
            case TYPE_KEY_PRESS_CMD:
                return ParcelableUtil.unmarshall(bytes, KeyPressCmd.CREATOR);
            case TYPE_SEND_FILE_CMD:
                return ParcelableUtil.unmarshall(bytes, SendFileCmd.CREATOR);
            case TYPE_PLAYER_CMD:
                return ParcelableUtil.unmarshall(bytes, PlayerCmd.CREATOR);
            case TYPE_PLAYLIST_CMD:
                return ParcelableUtil.unmarshall(bytes, PlayListCmd.CREATOR);
            case TYPE_INFO_CMD:
                return ParcelableUtil.unmarshall(bytes, InfoCmd.CREATOR);
            default:
                Log.e(TAG, "convertFromArray: unknown CMD type");
                break;
        }
        return null;
    }
}

