package com.grishberg.rowancommon.data.db;

/**
 * Created by grishberg on 19.06.16.
 */
import android.util.Log;

import com.grishberg.datafacade.data.ListResult;

import io.realm.Realm;
import io.realm.RealmModel;
import io.realm.RealmObject;
import io.realm.RealmResults;

/**
 * Реализация объекта - списка результатов из бд
 * позволяет добавить подписчиков, которые будут уведомлены, когда данные из
 * асинхронного запроса станут доступны.
 * Created by grishberg on 19.04.16.
 */
public class ListResultRealm<T extends RealmModel> extends BaseResultImpl<T>
        implements ListResult<T> {
    private static final String TAG = ListResultRealm.class.getSimpleName();

    public ListResultRealm(Realm realm, GetRequestListener<T> requestListener) {
        super(realm, requestListener);
    }

    /**
     * Вернуть элемент по индексу
     *
     * @param index
     * @return
     */
    @Override
    public T getItem(int index) {
        T item = realmResults.get(index);
        return item;
    }

    /**
     * Вернуть общее количество элементов
     *
     * @return
     */
    @Override
    public int getCount() {
        return realmResults.size();
    }

    /**
     * Возвращает состояние - загружены данные или нет
     *
     * @return
     */
    @Override
    public boolean isLoaded() {
        return realmResults.isLoaded();
    }

    @Override
    protected void onDataChanged(RealmResults<T> elements) {
        //stub
    }
}

