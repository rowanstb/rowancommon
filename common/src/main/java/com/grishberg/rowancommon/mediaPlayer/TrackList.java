package com.grishberg.rowancommon.mediaPlayer;

import android.os.Parcel;
import android.os.Parcelable;

import java.util.ArrayList;

import io.realm.RealmList;
import io.realm.RealmModel;
import io.realm.RealmObject;
import io.realm.annotations.Ignore;
import io.realm.annotations.PrimaryKey;
import io.realm.annotations.RealmClass;

/**
 * Created by grishberg on 19.06.16.
 * модель плейлиста
 */
@RealmClass
public class TrackList implements RealmModel, Parcelable{
    private static final String TAG = TrackList.class.getSimpleName();
    @PrimaryKey
    private String name;
    // массив привязок
    private RealmList<TrackRelation> tracks;

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public RealmList<TrackRelation> getTracks() {
        return tracks;
    }

    public void setTracks(RealmList<TrackRelation> tracks) {
        this.tracks = tracks;
    }

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeString(this.name);
        dest.writeList(this.tracks);
    }

    public TrackList() {
    }

    protected TrackList(Parcel in) {
        this.name = in.readString();
        this.tracks = new RealmList<>();
        in.readList(this.tracks, TrackItem.class.getClassLoader());
    }

    public static final Creator<TrackList> CREATOR = new Creator<TrackList>() {
        @Override
        public TrackList createFromParcel(Parcel source) {
            return new TrackList(source);
        }

        @Override
        public TrackList[] newArray(int size) {
            return new TrackList[size];
        }
    };
}
