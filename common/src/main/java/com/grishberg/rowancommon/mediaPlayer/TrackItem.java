package com.grishberg.rowancommon.mediaPlayer;

import android.os.Parcel;
import android.os.Parcelable;

import io.realm.RealmModel;
import io.realm.RealmObject;
import io.realm.annotations.PrimaryKey;
import io.realm.annotations.RealmClass;

/**
 * Created by grishberg on 19.06.16.
 * Модель элемента треклиста - трэк в очереди воспроизведения
 */
@RealmClass
public class TrackItem implements RealmModel, Parcelable {
    private static final String TAG = TrackItem.class.getSimpleName();
    @PrimaryKey
    private int id;
    private String fileName;
    private String artist;
    private String name;
    private String genre;
    private int duration;
    private boolean isEnabled;

    public String getFileName() {
        return fileName;
    }

    public void setFileName(String fileName) {
        this.fileName = fileName;
    }

    public String getArtist() {
        return artist;
    }

    public void setArtist(String artist) {
        this.artist = artist;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getGenre() {
        return genre;
    }

    public void setGenre(String genre) {
        this.genre = genre;
    }

    public int getDuration() {
        return duration;
    }

    public void setDuration(int duration) {
        this.duration = duration;
    }

    public boolean isEnabled() {
        return isEnabled;
    }

    public void setEnabled(boolean enabled) {
        isEnabled = enabled;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeInt(this.id);
        dest.writeString(this.fileName);
        dest.writeString(this.artist);
        dest.writeString(this.name);
        dest.writeString(this.genre);
        dest.writeInt(this.duration);
        dest.writeByte(this.isEnabled ? (byte) 1 : (byte) 0);
    }

    public TrackItem() {
    }

    protected TrackItem(Parcel in) {
        this.id = in.readInt();
        this.fileName = in.readString();
        this.artist = in.readString();
        this.name = in.readString();
        this.genre = in.readString();
        this.duration = in.readInt();
        this.isEnabled = in.readByte() != 0;
    }

    public static final Creator<TrackItem> CREATOR = new Creator<TrackItem>() {
        @Override
        public TrackItem createFromParcel(Parcel source) {
            return new TrackItem(source);
        }

        @Override
        public TrackItem[] newArray(int size) {
            return new TrackItem[size];
        }
    };
}

