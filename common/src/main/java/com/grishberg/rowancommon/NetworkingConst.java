package com.grishberg.rowancommon;

/**
 * Created by grishberg on 14.05.16.
 */
public class NetworkingConst {
    private static final String TAG = NetworkingConst.class.getSimpleName();
    public static final int BROADCAST_PORT = 5001;
    public static final int BACK_TCP_PORT = 5002;
    public static final int TCP_PORT = 5003;
}
