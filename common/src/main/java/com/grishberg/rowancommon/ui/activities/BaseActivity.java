package com.grishberg.rowancommon.ui.activities;

import android.content.ComponentName;
import android.content.Context;
import android.content.Intent;
import android.content.ServiceConnection;
import android.os.Bundle;
import android.os.Handler;
import android.os.IBinder;
import android.os.Looper;
import android.os.Message;
import android.support.annotation.Nullable;
import android.support.v7.app.AppCompatActivity;

import com.grishberg.rowancommon.data.services.HandlerReceiverImpl;
import com.grishberg.rowancommon.interfaces.BoundListener;
import com.grishberg.rowancommon.interfaces.FragmentsRegistrable;
import com.grishberg.rowancommon.interfaces.HandlerReceiver;

import java.util.LinkedList;
import java.util.List;

/**
 * Created by grishberg on 20.05.16.
 */
public abstract class BaseActivity<T extends HandlerReceiver> extends AppCompatActivity
        implements FragmentsRegistrable<T> {
    private static final String TAG = BaseActivity.class.getSimpleName();
    protected Intent bindServiceIntent;
    protected Handler handler = new Handler(Looper.getMainLooper()) {
        @Override
        public void handleMessage(Message msg) {
            super.handleMessage(msg);
            onMessageReceived(msg.what, msg.obj);
        }
    };
    protected boolean isBound;

    protected T rcService;
    private List<BoundListener> boundListeners = new LinkedList<>();

    protected abstract Intent getServiceIntent();

    @Override
    public void register(BoundListener listener) {
        boundListeners.add(listener);
    }

    @Override
    public void unregister(BoundListener listener) {
        boundListeners.remove(listener);
    }

    @Override
    public T getService() {
        return rcService;
    }

    private ServiceConnection connection = new ServiceConnection() {
        @Override
        public void onServiceConnected(ComponentName name, IBinder service) {
            if (!isFinishing()) {
                rcService = (T) ((HandlerReceiverImpl.LocalBinder) service).getService();
                rcService.registerHandler(handler);
                isBound = true;
                onBound();
            }
        }

        @Override
        public void onServiceDisconnected(ComponentName name) {
            isBound = false;
            if (rcService != null) {
                rcService.unregisterHandler(handler);
            }
            rcService = null;
        }
    };

    protected void onBound() {
        for (BoundListener listener : boundListeners) {
            listener.onServiceBound(rcService);
        }
    }

    protected void onUnbound() {
        for (BoundListener listener : boundListeners) {
            listener.onServiceUnBound(rcService);
        }
    }

    /**
     * Сообщение из сервиса
     *
     * @param what
     * @param obj
     */
    protected void onMessageReceived(int what, Object obj) {
        for (BoundListener listener : boundListeners) {
            listener.onMessageReceived(what, obj);
        }
    }

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        bindServiceIntent = getServiceIntent();
        startService(bindServiceIntent);
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
    }

    @Override
    protected void onStart() {
        super.onStart();
        bindToService();
    }

    @Override
    protected void onStop() {
        super.onStop();
        unbindFromService();
    }

    private void bindToService() {
        if (!isBound) {
            bindService(bindServiceIntent, connection, Context.BIND_AUTO_CREATE);
        }
    }

    private void unbindFromService() {
        if (isBound) {
            isBound = false;
            rcService.unregisterHandler(handler);
            unbindService(connection);
            rcService = null;
        }
    }
}
