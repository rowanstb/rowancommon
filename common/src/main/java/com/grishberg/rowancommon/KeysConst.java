package com.grishberg.rowancommon;

/**
 * Created by grishberg on 17.05.16.
 */
public class KeysConst {
    private static final String TAG = KeysConst.class.getSimpleName();
    public static final int KEY_ESC = 1;
    public static final int KEY_ENTER = 28;
    public static final int KEY_F1 = 59;
    public static final int KEY_F2 = 60;
    public static final int KEY_F3 = 61;
    public static final int KEY_F13 = 183;
    public static final int KEY_F5 = 63;
    public static final int KEY_UP = 103;
    public static final int KEY_LEFT = 105;
    public static final int KEY_RIGHT = 106;
    public static final int KEY_DOWN = 108;
    public static final int KEY_MUTE = 113;
    public static final int KEY_VOLUMEDOWN = 114;
    public static final int KEY_VOLUMEUP = 115;
    public static final int KEY_HOME = 102;//139;
    public static final int KEY_PAUSE = 164;
    public static final int KEY_FORWARD = 159;
    public static final int KEY_NEXTSONG = 163;
    public static final int KEY_PREVIOUSSONG = 165;
    public static final int KEY_REWIND = 168;
    public static final int KEY_INFO = 0x58;
    public static final int KEY_SCREEN = 0x177;
    public static final int KEY_RED = 0x18e;
    public static final int KEY_GREEN = 0x18f;
    public static final int KEY_YELLOW = 0x190;
    public static final int KEY_BLUE = 0x0191;
    public static final int KEY_CHANNEL_UP = 0x192;
    public static final int KEY_CHANNEL_DOWN = 0x193;
    //int KEY_CHANNEL_UP = 166;
    //int KEY_CHANNEL_DOWN = 167;
}
