package com.grishberg.rowancommon.interfaces;

import android.os.Handler;

/**
 * Created by grishberg on 20.05.16.
 */
public interface HandlerReceiver {
    void registerHandler(Handler handler);
    void unregisterHandler(Handler handler);
}
