package com.grishberg.rowancommon.interfaces;

import android.app.Service;

/**
 * Created by grishberg on 16.05.16.
 */
public interface BoundListener {
    void onServiceBound(HandlerReceiver service);
    void onServiceUnBound(HandlerReceiver service);
    void onMessageReceived(int what, Object obj);
}
