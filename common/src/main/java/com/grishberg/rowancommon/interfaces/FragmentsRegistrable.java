package com.grishberg.rowancommon.interfaces;

/**
 * Created by grishberg on 15.05.16.
 */
public interface FragmentsRegistrable<T> {
    void register(BoundListener listener);
    void unregister(BoundListener listener);
    T getService();
}
